module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: '7tual - Inicio',
    meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
        { hid: 'author', content: 'Handel Studio' },
        { hid: 'description', name: 'description', content: '7Tual - proyecto de eventos' }
    ],
    script: [
        { src: 'https://code.jquery.com/jquery-3.2.1.slim.min.js' },
        { src: 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js' },
        { src: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js' }
    ],
    link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        { rel: 'stylesheet', href:'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css' },
        { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:100,300,400,700|Quicksand:400,500,700' }
    ]
  },
  /*
  ** Plugins
  */
  plugins: [
    //{ src: '~/plugins/axios.js' },
    { src: '~/plugins/swiper.js', ssr: false }
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    },
    vendor: [
        'axios',
        'swiper'
    ]
  },
  css: [
    'swiper/dist/css/swiper.css'
  ]
};
